// Dark Mode
let btn = document.getElementById("dark_mode");
btn.addEventListener("click", function dark() {
  if (btn.value == "YES") {
    document.querySelector("#sun").className = "moon";
    btn.value = "NO";
    var dark = document.querySelector(".dark");
    dark.style.zIndex = "-1";
    document.querySelector("#sun").src = "../src/images/icon-sun.svg";
    document.body.style.backgroundColor = "rgb(24,24,36)";
  } else if (btn.value == "NO") {
    btn.value = "YES";
    document.querySelector("#sun").src = "../src/images/icon-moon.svg";
    var dark = document.querySelector(".dark");
    dark.style.zIndex = "-2";
    document.body.style.backgroundColor = "white";
    document.querySelector("#sun").className = "sun";
  }
});

let arr = [];
var count = 0;
// Input values adding in array
let InputData = document.getElementById("input");
InputData.addEventListener("keypress", function myFunction2(event) {
  if (event.key === "Enter") {
    if (InputData.value === "") {
      alert("Please enter Something");
    } else arr.push(InputData.value);
    console.log(arr);
    event.preventDefault();
    InputData.value = "";

    // Adding table Row
    const last = arr[arr.length - 1];
    let tbody = document.getElementById("tbody");
    let trow = " ";
    trow = `<tr id="clr" class="row1" >
        <td class="button1">
          <button id="check"  onclick="check1(this)"  value="YES">
            <img class="d-flex justify-content-center"  id="img1" src="../src/images/icon-check.svg"/>
          </button>
         <td> 
          <td><span id="done">${last}</span></td>
          <td class="ls">
           <button id="close-btn" class="imgbtn" onclick="delete1(this)" >
             <img class="cross" src="../src/images/icon-cross.svg">
           </button>
          </td>
        </tr>`;
    tbody.innerHTML += trow;
    tbody.appendChild(trow);
    document.getElementById("input").value = z;
    document.getElementById("check").disabled = true;
    count = document.getElementById("table").rows.length;
    console.log(count);
    document.getElementById("count").innerText = count;
    document.getElementById("all").style.color = "blue";
    document.getElementById("check").style.background =
      "linear-gradient(145deg, rgba(114,185,254,1) 23%, rgba(164,123,242,1) 67%)";
  }
});

function check1(e) {
  let check1 = document.getElementById("check1");
  let Trow = e.closest("tr");
  let child_2 = Trow.children[2];
  const child_1 = child_2.children[0];
  if (check1.value == "YES") {
    e.style.background = "none";
    child_1.className = "";
    //   Incomplete Tast
    var element = document.getElementById("Incomplete");
    element.addEventListener("click", myFunction2);
    function myFunction2() {
      if (check1.value == "YES") {
        e.closest("tr").style.display = "none";
        document.getElementById("all").style.color = "black";
        element.style.color = "blue";
      } else if (check1.value == "NO") {
        e.closest("tr").style.display = "";
      }
      }
      check1.value = "NO";
  } else {
    e.style.background =
      "linear-gradient(145deg, rgba(114,185,254,1) 23%, rgba(164,123,242,1) 67%)";
   

    //   All Tast
    var element2 = document.getElementById("all");
    element2.addEventListener("click", myFunction2);
    function myFunction2() {
      if (check1.value == "YES") {
        e.closest("tr").style.display = "";
        element2.style.color = "blue";
        document.getElementById("Incomplete").style.color = "black";
        }
        
    }
    // check tast is completed classname
    child_1.className = "del";
    // clear all completed
    var clear = document.getElementById("clear");
    clear.addEventListener("click", myFunction1);
    function myFunction1() {
      let check1 = document.getElementById("check1");
      console.log("clear");
      if (check1.value == "NO") {
      }
      e.closest("tr").remove();
      }
       check1.value = "YES";
  }
}

//delete

function delete1(e) {
  e.closest("tr").remove();
}
